CC = gcc
CFLAGS = -Wall -O2
GTK3_CFLAGS = `pkg-config --cflags gtk+-3.0`
GTK3_LIBS = `pkg-config --libs gtk+-3.0`
OBJS = main.o
SRC = main.c
OUTPUT = anycolour


all: $(OUTPUT)


$(OUTPUT): $(OBJS)
	$(CC) $(OBJS) $(GTK3_LIBS) -o $(OUTPUT)


$(OBJS): $(SRC)
	$(CC) $(GTK3_CFLAGS) $(CFLAGS) -c -o $@ $(SRC)


clean:
	rm -f $(OUTPUT) *.o
